package com.snaprix.thenewmotion.presenter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;

import com.snaprix.data.repository.UserRepositoryImpl;
import com.snaprix.domain.model.User;
import com.snaprix.domain.repository.UserRepository;
import com.snaprix.domain.usecase.GetUserUsecase;
import com.snaprix.domain.usecase.GetUserUsecaseImpl;
import com.snaprix.thenewmotion.R;
import com.snaprix.thenewmotion.activity.LoginActivity;
import com.snaprix.thenewmotion.activity.MainActivity;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by vladimirryabchikov on 7/7/15.
 */
public class MainPresenter implements Presenter<MainActivity> {
    private static final int REQUEST_CODE_LOGIN = 1;

    private MainActivity view;
    private PendingActivityResult pendingActivityResult;

    private GetUserUsecase getUserUsecase;

    @Override
    public void onViewCreated(MainActivity view, Bundle savedInstanceState) {
        this.view = view;

        UserRepository userRepository = new UserRepositoryImpl(view.getContext());
        getUserUsecase = new GetUserUsecaseImpl(userRepository);

        if (savedInstanceState == null) {
            getUser(false);
        }
    }

    @Override
    public void onResume() {
        if (pendingActivityResult != null) {
            switch (pendingActivityResult.requestCode) {
                case REQUEST_CODE_LOGIN:
                    if (pendingActivityResult.resultCode == Activity.RESULT_OK) {
                        getUser(true);
                    } else {
                        view.finish();
                    }
                    break;
                default:
                    break;
            }

            pendingActivityResult = null;
        }
    }

    @Override
    public void onPause() {

    }

    @Override
    public void onViewDestroyed() {
        view = null;
    }

    @Override
    public boolean isViewDestroyed() {
        return view == null;
    }

    public boolean onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CODE_LOGIN:
                pendingActivityResult = new PendingActivityResult(requestCode, resultCode, data);
                return true;
            default:
                return false;
        }
    }

    private void getUser(final boolean showWelcome) {
        getUserUsecase.execute()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<User>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        login();
                    }

                    @Override
                    public void onNext(User user) {
                        if (showWelcome) {
                            new AlertDialog.Builder(view.getContext())
                                    .setTitle(view.getString(R.string.welcome_title, user.getFirstName(), user.getLastName()))
                                    .setMessage(view.getString(R.string.welcome_text))
                                    .setPositiveButton(android.R.string.ok, null)
                                    .show();
                        }

                        MainPresenter.this.view.showChargePoints();
                    }
                });
    }

    private void login() {
        Intent i = LoginActivity.newIntent(view);
        view.startActivityForResult(i, REQUEST_CODE_LOGIN);
    }



    private static class PendingActivityResult {
        private final int requestCode;
        private final int resultCode;
        private final Intent data;

        public PendingActivityResult(int requestCode, int resultCode, Intent data) {
            this.requestCode = requestCode;
            this.resultCode = resultCode;
            this.data = data;
        }
    }
}