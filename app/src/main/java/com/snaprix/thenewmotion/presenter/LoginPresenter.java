package com.snaprix.thenewmotion.presenter;

import android.os.Bundle;
import android.util.Log;

import com.snaprix.data.repository.UserRepositoryImpl;
import com.snaprix.domain.exception.UnauthorizedException;
import com.snaprix.domain.model.User;
import com.snaprix.domain.repository.UserRepository;
import com.snaprix.domain.usecase.LoginUsecase;
import com.snaprix.domain.usecase.LoginUsecaseImpl;
import com.snaprix.thenewmotion.R;
import com.snaprix.thenewmotion.fragment.LoginFragment;
import com.snaprix.thenewmotion.utils.LogUtils;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by vladimirryabchikov on 7/7/15.
 */
public class LoginPresenter implements Presenter<LoginFragment> {
    private static final boolean DEBUG = LogUtils.DEBUG;
    private static final String TAG = "LoginPresenter";

    private LoginFragment view;

    @Override
    public void onViewCreated(LoginFragment view, Bundle savedInstanceState) {
        this.view = view;
    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onViewDestroyed() {
        view = null;
    }

    @Override
    public boolean isViewDestroyed() {
        return view == null;
    }

    public void onLoginClick() {
        String username = view.getName();
        String password = view.getPassword();

        if (DEBUG) Log.v(TAG, String.format("onLoginClick username: %s password: %s",
                username, password));

        if (username.isEmpty()) {
            view.showError(R.string.username_error);
            return;
        }

        if (password.isEmpty()) {
            view.showError(R.string.password_error);
            return;
        }

        view.showProgress();

        UserRepository userRepository = new UserRepositoryImpl(view.getContext());
        LoginUsecase usecase = new LoginUsecaseImpl(userRepository);
        usecase.execute(username, password)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<User>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable ex) {
                        if (isViewDestroyed()) return;

                        if (DEBUG) Log.e(TAG, "onError", ex);

                        view.hideProgress();
                        if (ex instanceof UnauthorizedException) {
                            view.showError(R.string.login_error);
                        } else {
                            view.showError(R.string.login_error_unknown);
                        }
                    }

                    @Override
                    public void onNext(User user) {
                        if (isViewDestroyed()) return;

                        view.hideProgress();
                        view.finish();
                    }
                });
    }
}