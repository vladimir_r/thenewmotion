package com.snaprix.thenewmotion.presenter;

import android.os.Bundle;

/**
 * Created by vladimirryabchikov on 7/7/15.
 */
public interface Presenter<T> {
    void onViewCreated(T view, Bundle savedInstanceState);
    void onResume();
    void onPause();
    void onViewDestroyed();

    boolean isViewDestroyed();
}