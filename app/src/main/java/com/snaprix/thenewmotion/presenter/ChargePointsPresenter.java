package com.snaprix.thenewmotion.presenter;

import android.os.Bundle;

import com.snaprix.data.repository.ChargePointRepositoryImpl;
import com.snaprix.domain.model.ChargePoint;
import com.snaprix.domain.repository.ChargePointRepository;
import com.snaprix.domain.usecase.GetChargePointsUsecase;
import com.snaprix.domain.usecase.GetChargePointsUsecaseImpl;
import com.snaprix.thenewmotion.fragment.ChargePointsMapFragment;

import java.util.List;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by vladimirryabchikov on 7/7/15.
 */
public class ChargePointsPresenter implements Presenter<ChargePointsMapFragment> {
    private static final String STATE_FOCUSED = "state.focused";

    private ChargePointsMapFragment view;
    private boolean isFocused;

    @Override
    public void onViewCreated(final ChargePointsMapFragment view, final Bundle savedInstanceState) {
        this.view = view;

        ChargePointRepository repository = new ChargePointRepositoryImpl(view.getContext());
        GetChargePointsUsecase usecase = new GetChargePointsUsecaseImpl(repository);
        usecase.execute()
                .toList()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<ChargePoint>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(List<ChargePoint> chargePoints) {
                        if (isViewDestroyed()) return;

                        isFocused = savedInstanceState != null && savedInstanceState.getBoolean(STATE_FOCUSED);

                        if (!isFocused && chargePoints.size() > 0) {
                            ChargePoint firstChargePoint = chargePoints.get(0);
                            view.focus(firstChargePoint.getLat(), firstChargePoint.getLng(), ChargePointsMapFragment.ZOOM_CITY);
                            isFocused = true;
                        }

                        view.showChargePoints(chargePoints);
                    }
                });
    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onViewDestroyed() {
        this.view = null;
    }

    public void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(STATE_FOCUSED, isFocused);
    }

    @Override
    public boolean isViewDestroyed() {
        return view == null;
    }
}