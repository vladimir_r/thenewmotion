package com.snaprix.thenewmotion.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.snaprix.thenewmotion.R;
import com.snaprix.thenewmotion.fragment.LoginFragment;

/**
 * Created by vladimirryabchikov on 7/7/15.
 */
public class LoginActivity extends AppCompatActivity implements LoginFragment.Callback{
    public static Intent newIntent(Context context){
        return new Intent(context, LoginActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    @Override
    public void onLoginSuccess() {
        setResult(RESULT_OK);
        finish();
    }
}