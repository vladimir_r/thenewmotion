package com.snaprix.thenewmotion.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.snaprix.thenewmotion.R;
import com.snaprix.thenewmotion.presenter.LoginPresenter;

/**
 * Created by vladimirryabchikov on 7/7/15.
 */
public class LoginFragment extends Fragment {
    private static final String TEST_USERNAME = "programming-assignment@thenewmotion.com";
    private static final String TEST_PASSWORD = "Zea2E5RA";

    private EditText usernameView;
    private EditText passwordView;
    private Button loginView;
    private Button forgetView;
    private ProgressDialog progressDialog;

    private LoginPresenter presenter;
    private Callback callback;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        callback = (Callback) activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        usernameView = (EditText) view.findViewById(R.id.username_editText);
        passwordView = (EditText) view.findViewById(R.id.password_editText);
        loginView = (Button) view.findViewById(R.id.login_button);
        forgetView = (Button) view.findViewById(R.id.forget_textView);

        loginView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onLoginClick();
            }
        });

        forgetView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                usernameView.setText(TEST_USERNAME);
                passwordView.setText(TEST_PASSWORD);
            }
        });

        presenter = new LoginPresenter();
        presenter.onViewCreated(this, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    public void onPause() {
        presenter.onPause();
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        presenter.onViewDestroyed();
        super.onDestroyView();
    }

    public Context getContext(){
        return getActivity();
    }

    public String getName() {
        return usernameView.getText().toString();
    }

    public String getPassword() {
        return passwordView.getText().toString();
    }

    public void showProgress(){
        if (progressDialog != null) return;

        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage(getContext().getString(R.string.loading));
        progressDialog.show();
    }

    public void hideProgress(){
        if (progressDialog == null) return;

        progressDialog.hide();
        progressDialog = null;
    }

    public void showError(int stringResId){
        if (!isAdded()) return;

        Toast.makeText(getActivity(), stringResId, Toast.LENGTH_LONG).show();
    }

    public void finish(){
        if (!isAdded()) return;

        callback.onLoginSuccess();
    }



    public interface Callback {
        void onLoginSuccess();
    }
}