package com.snaprix.thenewmotion.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.snaprix.domain.model.ChargePoint;
import com.snaprix.thenewmotion.presenter.ChargePointsPresenter;

import java.util.List;

/**
 * Created by vladimirryabchikov on 7/7/15.
 */
public class ChargePointsMapFragment extends SupportMapFragment {
    public static final float ZOOM_CITY = 10f;

    public static ChargePointsMapFragment newInstance(){
        return new ChargePointsMapFragment();
    }

    private ChargePointsPresenter presenter;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter = new ChargePointsPresenter();
        presenter.onViewCreated(this, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    public void onPause() {
        presenter.onPause();
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        presenter.onViewDestroyed();
        super.onDestroyView();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        presenter.onSaveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    public Context getContext(){
        return getActivity();
    }

    public void focus(double lat, double lng, float zoom) {
        getMap().animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), zoom));
    }

    public void showChargePoints(List<ChargePoint> points) {
        for (ChargePoint point : points) {
            MarkerOptions mo = new MarkerOptions()
                    .position(new LatLng(point.getLat(), point.getLng()))
                    .title(String.format("%s, %s", point.getAddress(), point.getCity()));

            getMap().addMarker(mo);
        }
    }
}