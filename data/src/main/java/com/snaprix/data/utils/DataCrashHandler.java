package com.snaprix.data.utils;

import android.util.Log;

/**
 * Created by vladimirryabchikov on 7/8/15.
 */
public class DataCrashHandler {
    public static void notFatalException(String tag, String message, Exception ex){
        // TODO send to Analytics service
        Log.w(tag, message, ex);
    }
}