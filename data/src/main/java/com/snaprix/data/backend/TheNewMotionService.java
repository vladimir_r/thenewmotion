package com.snaprix.data.backend;

import com.snaprix.data.entity.TokenEntity;
import com.snaprix.data.entity.UserEntity;

import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Headers;
import retrofit.http.POST;
import rx.Observable;

/**
 * Created by vladimirryabchikov on 7/8/15.
 */
public interface TheNewMotionService {
    String GRANT_TYPE = "password";

    @FormUrlEncoded
    @Headers({
            "Content-Type: application/x-www-form-urlencoded",
            "Authorization: Basic dGVzdF9jbGllbnRfaWQ6dGVzdF9jbGllbnRfc2VjcmV0"
    })
    @POST("/oauth2/access_token")
    Observable<TokenEntity> accessToken(@Field("grant_type") String grantType, @Field("username") String username, @Field("password") String password);


    @GET("/v1/me")
    Observable<UserEntity> me(@Header("Authorization") String authorization);
}