package com.snaprix.data.repository;

import android.content.Context;

import com.snaprix.data.datastore.LocalChargePointDataStore;
import com.snaprix.domain.model.ChargePoint;
import com.snaprix.domain.repository.ChargePointRepository;

import rx.Observable;
import rx.schedulers.Schedulers;

/**
 * Created by vladimirryabchikov on 7/7/15.
 */
public class ChargePointRepositoryImpl implements ChargePointRepository {
    private final LocalChargePointDataStore localStore;

    public ChargePointRepositoryImpl(Context context) {
        localStore = new LocalChargePointDataStore(context);
    }

    @Override
    public Observable<ChargePoint> getChargePoints() {
        return localStore.getChargePoints()
                .subscribeOn(Schedulers.computation());
    }
}