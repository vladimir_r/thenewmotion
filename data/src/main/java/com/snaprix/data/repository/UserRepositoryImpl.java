package com.snaprix.data.repository;

import android.content.Context;

import com.snaprix.data.datastore.LocalUserDataStore;
import com.snaprix.data.datastore.ServerUserDataStore;
import com.snaprix.domain.model.Token;
import com.snaprix.domain.model.User;
import com.snaprix.domain.repository.UserRepository;

import rx.Observable;
import rx.functions.Func1;

/**
 * Created by vladimirryabchikov on 7/7/15.
 */
public class UserRepositoryImpl implements UserRepository {
    private final LocalUserDataStore localStore;
    private final ServerUserDataStore serverStore;

    public UserRepositoryImpl(Context context) {
        localStore = new LocalUserDataStore(context);
        serverStore = new ServerUserDataStore();
    }

    @Override
    public Observable<User> getUser() {
        return localStore.getUser();
    }

    @Override
    public Observable<User> login(String name, String password) {
        return serverStore.login(name, password)
                .flatMap(new Func1<Token, Observable<User>>() {
                    @Override
                    public Observable<User> call(Token token) {
                        return serverStore.getUser(token);
                    }
                })
                .flatMap(new Func1<User, Observable<User>>() {
                    @Override
                    public Observable<User> call(User user) {
                        return localStore.setUser(user);
                    }
                });
    }
}