package com.snaprix.data.datastore;

import android.content.Context;
import android.content.res.Resources;

import com.google.gson.GsonBuilder;
import com.snaprix.data.R;
import com.snaprix.data.entity.ChargePointEntity;
import com.snaprix.data.mapper.ChargePointMapper;
import com.snaprix.domain.model.ChargePoint;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import rx.Observable;
import rx.Subscriber;
import rx.functions.Func1;

/**
 * Created by vladimirryabchikov on 7/8/15.
 */
public class LocalChargePointDataStore {
    private final Context context;

    public LocalChargePointDataStore(Context context) {
        this.context = context;
    }

    public Observable<ChargePoint> getChargePoints() {
        final ChargePointMapper mapper = new ChargePointMapper();

        return Observable.create(new Observable.OnSubscribe<String>() {
            @Override
            public void call(Subscriber<? super String> subscriber) {
                try {
                    String json = readRawRes(context, R.raw.sample_json_chargepoints);
                    subscriber.onNext(json);
                    subscriber.onCompleted();
                } catch (IOException e) {
                    subscriber.onError(e);
                }
            }
        })
                .flatMap(new Func1<String, Observable<ChargePointEntity>>() {
                    @Override
                    public Observable<ChargePointEntity> call(String s) {
                        ChargePointEntity[] list = new GsonBuilder().create().fromJson(s, ChargePointEntity[].class);
                        return Observable.from(list);
                    }
                })
                .map(new Func1<ChargePointEntity, ChargePoint>() {
                    @Override
                    public ChargePoint call(ChargePointEntity chargePointEntity) {
                        return mapper.map(chargePointEntity);
                    }
                });
    }

    private String readRawRes(Context context, int id) throws IOException {
        Resources res = context.getResources();
        InputStream inputStream = res.openRawResource(id);

        BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder total = new StringBuilder();

        String line;
        try {
            while ((line = r.readLine()) != null) {
                total.append(line);
            }
        } finally {
            r.close();
        }
        return total.toString();
    }
}