package com.snaprix.data.datastore;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.snaprix.domain.model.User;

import rx.Observable;

/**
 * Created by vladimirryabchikov on 7/8/15.
 */
public class LocalUserDataStore {
    private static final String KEY_USER = "key.user";

    private final SharedPreferences sharedPreferences;
    private final Gson gson;

    public LocalUserDataStore(Context context) {
        sharedPreferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
        gson = new GsonBuilder().create();
    }

    public Observable<User> getUser() {
        if (!sharedPreferences.contains(KEY_USER)) {
            return Observable.error(new Exception());
        }

        return Observable.just(gson.fromJson(sharedPreferences.getString(KEY_USER, null), User.class));
    }

    public Observable<User> setUser(User user) {
        sharedPreferences.edit()
                .putString(KEY_USER, gson.toJson(user))
                .apply();

        return Observable.just(user);
    }
}