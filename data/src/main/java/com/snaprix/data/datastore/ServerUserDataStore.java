package com.snaprix.data.datastore;

import com.snaprix.data.backend.TheNewMotionService;
import com.snaprix.data.entity.TokenEntity;
import com.snaprix.data.entity.UserEntity;
import com.snaprix.data.mapper.TokenMapper;
import com.snaprix.data.mapper.UserMapper;
import com.snaprix.domain.exception.UnauthorizedException;
import com.snaprix.domain.model.Token;
import com.snaprix.domain.model.User;

import retrofit.ErrorHandler;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import rx.Observable;
import rx.functions.Func1;

/**
 * Created by vladimirryabchikov on 7/8/15.
 */
public class ServerUserDataStore {
    private static final int HTTP_UNAUTHORIZED = 401;

    private TheNewMotionService service;

    public ServerUserDataStore(){
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setErrorHandler(new CustomErrorHandler())
                .setEndpoint("https://test-api.thenewmotion.com")
                .build();

        service = restAdapter.create(TheNewMotionService.class);
    }

    public Observable<User> getUser(final Token token) {
        return service.me(token.getType() + " " + token.getAccess())
                .map(new Func1<UserEntity, User>() {
                    @Override
                    public User call(UserEntity userEntity) {
                        return new UserMapper(token).map(userEntity);
                    }
                });
    }

    public Observable<Token> login(String name, String password) {
        return service.accessToken(TheNewMotionService.GRANT_TYPE, name, password)
                .map(new Func1<TokenEntity, Token>() {
                    @Override
                    public Token call(TokenEntity tokenEntity) {
                        return new TokenMapper().map(tokenEntity);
                    }
                });
    }

    class CustomErrorHandler implements ErrorHandler {
        @Override
        public Throwable handleError(RetrofitError cause) {
            Response r = cause.getResponse();
            if (r != null && r.getStatus() == HTTP_UNAUTHORIZED) {
                return new UnauthorizedException(cause);
            }
            return cause;
        }
    }
}