package com.snaprix.data.mapper;

/**
 * Created by vladimirryabchikov on 7/8/15.
 */
public interface Mapper<FROM, TO> {
    TO map(FROM from);
}