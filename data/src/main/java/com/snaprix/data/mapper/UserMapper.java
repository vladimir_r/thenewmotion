package com.snaprix.data.mapper;

import com.snaprix.data.entity.UserEntity;
import com.snaprix.domain.model.Token;
import com.snaprix.domain.model.User;

/**
 * Created by vladimirryabchikov on 7/8/15.
 */
public class UserMapper implements Mapper<UserEntity, User> {
    private final Token token;

    public UserMapper(Token token) {
        this.token = token;
    }

    @Override
    public User map(UserEntity userEntity) {
        return new User(userEntity.id, userEntity.firstName, userEntity.lastName, userEntity.email, token);
    }
}