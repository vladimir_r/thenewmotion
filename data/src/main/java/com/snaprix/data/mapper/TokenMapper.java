package com.snaprix.data.mapper;

import com.snaprix.data.entity.TokenEntity;
import com.snaprix.domain.model.Token;

/**
 * Created by vladimirryabchikov on 7/8/15.
 */
public class TokenMapper implements Mapper<TokenEntity, Token> {
    @Override
    public Token map(TokenEntity tokenEntity) {
        return new Token(tokenEntity.token_type, tokenEntity.expires_in, tokenEntity.access_token, tokenEntity.refresh_token);
    }
}
