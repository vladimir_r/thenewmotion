package com.snaprix.data.mapper;

import com.snaprix.data.entity.ChargePointEntity;
import com.snaprix.data.entity.ConnectorEntity;
import com.snaprix.data.entity.PowerEntity;
import com.snaprix.data.entity.PriceEntity;
import com.snaprix.data.utils.DataCrashHandler;
import com.snaprix.domain.model.ChargePoint;
import com.snaprix.domain.model.Connector;
import com.snaprix.domain.model.Power;
import com.snaprix.domain.model.Price;

/**
 * Created by vladimirryabchikov on 7/8/15.
 */
public class ChargePointMapper implements Mapper<ChargePointEntity, ChargePoint> {
    private static final String TAG = "ChargePointMapper";

    @Override
    public ChargePoint map(ChargePointEntity chargePointEntity) {
        Connector[] connectors = new Connector[chargePointEntity.connectors.length];

        ConnectorEntity[] connectorsEntities = chargePointEntity.connectors;
        for (int i = 0; i < connectorsEntities.length; i++) {
            ConnectorEntity connectorEntity = connectorsEntities[i];

            PowerEntity powerEntity = connectorEntity.power;
            final int current;
            switch (powerEntity.current){
                case PowerEntity.CURRENT_AC:
                    current = Power.CURRENT_AC;
                    break;
                case PowerEntity.CURRENT_DC:
                    current = Power.CURRENT_DC;
                    break;
                default:
                    DataCrashHandler.notFatalException(TAG, "power", new UnsupportedOperationException("current:" + powerEntity.current));
                    current = Power.CURRENT_DEFAULT;
                    break;
            }

            Power power = new Power(current, powerEntity.phase, powerEntity.voltage, powerEntity.amperage);

            PriceEntity priceEntity = connectorEntity.price;
            Price price = null;
            if (priceEntity != null) {
                final int currency;
                switch (priceEntity.currency){
                    case PriceEntity.CURRENT_EUR:
                        currency = Price.CURRENCY_EUR;
                        break;
                    default:
                        DataCrashHandler.notFatalException(TAG, "price", new UnsupportedOperationException("currency:" + priceEntity.currency));
                        currency = Price.CURRENCY_DEFAULT;
                        break;
                }
                price = new Price(priceEntity.perSession, priceEntity.perMinute, priceEntity.perKWh, currency);
            }

            final int type;
            switch (connectorEntity.connectorType){
                case ConnectorEntity.CONNECTOR_TYPE_TYPE_2:
                    type = Connector.CONNECTOR_TYPE_TYPE_2;
                    break;
                case ConnectorEntity.CONNECTOR_TYPE_TEPCO_CH_ADE_MO:
                    type = Connector.CONNECTOR_TYPE_TEPCO_CH_ADE_MO;
                    break;
                case ConnectorEntity.CONNECTOR_TYPE_TYPE_2_COMBO:
                    type = Connector.CONNECTOR_TYPE_TYPE_2_COMBO;
                    break;
                default:
                    DataCrashHandler.notFatalException(TAG, "connector", new UnsupportedOperationException("type:" + connectorEntity.connectorType));
                    type = Connector.CONNECTOR_TYPE_DEFAULT;
                    break;
            }
            connectors[i] = new Connector(connectorEntity.id, type, power, price);
        }

        return new ChargePoint(chargePointEntity.id, chargePointEntity.city, chargePointEntity.address,
                chargePointEntity.lat, chargePointEntity.lng, connectors);
    }
}