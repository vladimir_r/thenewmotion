package com.snaprix.data.entity;

/**
 * Created by vladimirryabchikov on 7/7/15.
 */
public class PowerEntity {
    public static final String CURRENT_AC = "AC";
    public static final String CURRENT_DC = "DC";

    public String current;
    public int phase;
    public int voltage;
    public int amperage;
}