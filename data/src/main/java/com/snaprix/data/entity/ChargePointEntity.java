package com.snaprix.data.entity;

/**
 * Created by vladimirryabchikov on 7/7/15.
 */
public class ChargePointEntity {
    public int id;
    public String city;
    public String address;
    public double lat;
    public double lng;
    public ConnectorEntity[] connectors;
}