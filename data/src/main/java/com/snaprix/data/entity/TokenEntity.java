package com.snaprix.data.entity;

/**
 * Created by vladimirryabchikov on 7/7/15.
 */
public class TokenEntity {
    public String token_type;
    public long expires_in;
    public String access_token;
    public String refresh_token;
}