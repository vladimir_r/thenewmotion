package com.snaprix.data.entity;

/**
 * Created by vladimirryabchikov on 7/7/15.
 */
public class UserEntity {
    public String id;
    public String firstName;
    public String lastName;
    public String email;
}