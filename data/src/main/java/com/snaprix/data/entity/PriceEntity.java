package com.snaprix.data.entity;

/**
 * Created by vladimirryabchikov on 7/7/15.
 */
public class PriceEntity {
    public static final String CURRENT_EUR = "EUR";

    public double perSession;
    public double perMinute;
    public double perKWh;
    public String currency;
}