package com.snaprix.data.entity;

/**
 * Created by vladimirryabchikov on 7/7/15.
 */
public class ConnectorEntity {
    public static final String CONNECTOR_TYPE_TYPE_2 = "Type2";
    public static final String CONNECTOR_TYPE_TEPCO_CH_ADE_MO = "TepcoCHAdeMO";
    public static final String CONNECTOR_TYPE_TYPE_2_COMBO = "Type2Combo";

    public int id;
    public String connectorType;
    public PowerEntity power;
    public PriceEntity price;
}