package com.snaprix.domain.model;

/**
 * Created by vladimirryabchikov on 7/7/15.
 */
public class Power {
    public static final int CURRENT_AC = 1;
    public static final int CURRENT_DC = 2;
    public static final int CURRENT_DEFAULT = CURRENT_AC;

    private final int current;
    private final int phase;
    private final int voltage;
    private final int amperage;

    public Power(int current, int phase, int voltage, int amperage) {
        this.current = current;
        this.phase = phase;
        this.voltage = voltage;
        this.amperage = amperage;
    }

    public int getCurrent() {
        return current;
    }

    public int getPhase() {
        return phase;
    }

    public int getVoltage() {
        return voltage;
    }

    public int getAmperage() {
        return amperage;
    }
}