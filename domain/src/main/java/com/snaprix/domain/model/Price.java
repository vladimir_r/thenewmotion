package com.snaprix.domain.model;

/**
 * Created by vladimirryabchikov on 7/7/15.
 */
public class Price {
    public static final int CURRENCY_EUR = 1;
    public static final int CURRENCY_DEFAULT = CURRENCY_EUR;

    private final double perSession;
    private final double perMinute;
    private final double perKWh;
    private final int currency;

    public Price(double perSession, double perMinute, double perKWh, int currency) {
        this.perSession = perSession;
        this.perMinute = perMinute;
        this.perKWh = perKWh;
        this.currency = currency;
    }

    public double getPerSession() {
        return perSession;
    }

    public double getPerMinute() {
        return perMinute;
    }

    public double getPerKWh() {
        return perKWh;
    }

    public int getCurrency() {
        return currency;
    }
}