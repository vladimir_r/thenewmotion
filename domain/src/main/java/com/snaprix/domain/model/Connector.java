package com.snaprix.domain.model;

/**
 * Created by vladimirryabchikov on 7/7/15.
 */
public class Connector {
    public static final int CONNECTOR_TYPE_TYPE_2 = 1;
    public static final int CONNECTOR_TYPE_TEPCO_CH_ADE_MO = 2;
    public static final int CONNECTOR_TYPE_TYPE_2_COMBO = 3;
    public static final int CONNECTOR_TYPE_DEFAULT = CONNECTOR_TYPE_TYPE_2;

    private final int id;
    private final int connectorType;
    private final Power power;
    private final Price price;

    public Connector(int id, int connectorType, Power power, Price price) {
        this.id = id;
        this.connectorType = connectorType;
        this.power = power;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public int getConnectorType() {
        return connectorType;
    }

    public Power getPower() {
        return power;
    }

    public Price getPrice() {
        return price;
    }
}