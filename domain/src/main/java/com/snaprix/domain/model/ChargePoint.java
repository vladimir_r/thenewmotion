package com.snaprix.domain.model;

/**
 * Created by vladimirryabchikov on 7/7/15.
 */
public class ChargePoint {
    private final int id;
    private final String city;
    private final String address;
    private final double lat;
    private final double lng;
    private final Connector[] connectors;

    public ChargePoint(int id, String city, String address, double lat, double lng, Connector[] connectors) {
        this.id = id;
        this.city = city;
        this.address = address;
        this.lat = lat;
        this.lng = lng;
        this.connectors = connectors;
    }

    public int getId() {
        return id;
    }

    public String getCity() {
        return city;
    }

    public String getAddress() {
        return address;
    }

    public double getLat() {
        return lat;
    }

    public double getLng() {
        return lng;
    }

    public Connector[] getConnectors() {
        return connectors;
    }
}