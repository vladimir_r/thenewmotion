package com.snaprix.domain.model;

/**
 * Created by vladimirryabchikov on 7/7/15.
 */
public class Token {
    private final String type;
    private final long lifetimeSeconds;
    private final String access;
    private final String refresh;

    public Token(String type, long lifetimeSeconds, String access, String refresh) {
        this.type = type;
        this.lifetimeSeconds = lifetimeSeconds;
        this.access = access;
        this.refresh = refresh;
    }

    public String getType() {
        return type;
    }

    public long getLifetimeSeconds() {
        return lifetimeSeconds;
    }

    public String getAccess() {
        return access;
    }

    public String getRefresh() {
        return refresh;
    }
}