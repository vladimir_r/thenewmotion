package com.snaprix.domain.model;

/**
 * Created by vladimirryabchikov on 7/7/15.
 */
public class User {
    private final String id;
    private final String firstName;
    private final String lastName;
    private final String email;
    private final Token token;

    public User(String id, String firstName, String lastName, String email, Token token) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.token = token;
    }

    public String getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public Token getToken() {
        return token;
    }
}