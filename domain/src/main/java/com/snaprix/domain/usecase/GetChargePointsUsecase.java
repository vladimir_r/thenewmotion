package com.snaprix.domain.usecase;

import com.snaprix.domain.model.ChargePoint;

import rx.Observable;

/**
 * Created by vladimirryabchikov on 7/7/15.
 */
public interface GetChargePointsUsecase {
    Observable<ChargePoint> execute();
}