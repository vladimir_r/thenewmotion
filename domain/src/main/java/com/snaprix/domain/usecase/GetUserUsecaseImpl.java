package com.snaprix.domain.usecase;

import com.snaprix.domain.model.User;
import com.snaprix.domain.repository.UserRepository;

import rx.Observable;

/**
 * Created by vladimirryabchikov on 7/7/15.
 */
public class GetUserUsecaseImpl implements GetUserUsecase {
    private final UserRepository repository;

    public GetUserUsecaseImpl(UserRepository repository) {
        this.repository = repository;
    }

    @Override
    public Observable<User> execute() {
        return repository.getUser();
    }
}