package com.snaprix.domain.usecase;

import com.snaprix.domain.model.User;
import com.snaprix.domain.repository.UserRepository;

import rx.Observable;

/**
 * Created by vladimirryabchikov on 7/7/15.
 */
public class LoginUsecaseImpl implements LoginUsecase {
    private final UserRepository repository;

    public LoginUsecaseImpl(UserRepository repository) {
        this.repository = repository;
    }

    @Override
    public Observable<User> execute(String name, String password) {
        return repository.login(name, password);
    }
}