package com.snaprix.domain.usecase;

import com.snaprix.domain.model.User;

import rx.Observable;

/**
 * Created by vladimirryabchikov on 7/7/15.
 */
public interface LoginUsecase {
    Observable<User> execute(String name, String password);
}