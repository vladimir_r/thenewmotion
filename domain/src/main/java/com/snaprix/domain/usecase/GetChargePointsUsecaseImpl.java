package com.snaprix.domain.usecase;

import com.snaprix.domain.model.ChargePoint;
import com.snaprix.domain.repository.ChargePointRepository;

import rx.Observable;

/**
 * Created by vladimirryabchikov on 7/7/15.
 */
public class GetChargePointsUsecaseImpl implements GetChargePointsUsecase {
    private final ChargePointRepository repository;

    public GetChargePointsUsecaseImpl(ChargePointRepository repository) {
        this.repository = repository;
    }

    @Override
    public Observable<ChargePoint> execute() {
        return repository.getChargePoints();
    }
}