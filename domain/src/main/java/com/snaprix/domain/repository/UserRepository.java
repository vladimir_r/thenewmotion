package com.snaprix.domain.repository;

import com.snaprix.domain.model.User;

import rx.Observable;

/**
 * Created by vladimirryabchikov on 7/7/15.
 */
public interface UserRepository {
    Observable<User> getUser();
    Observable<User> login(String name, String password);
}