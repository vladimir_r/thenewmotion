package com.snaprix.domain.repository;

import com.snaprix.domain.model.ChargePoint;

import rx.Observable;

/**
 * Created by vladimirryabchikov on 7/7/15.
 */
public interface ChargePointRepository {
    Observable<ChargePoint> getChargePoints();
}